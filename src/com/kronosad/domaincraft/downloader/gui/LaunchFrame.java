package com.kronosad.domaincraft.downloader.gui;


import com.kronosad.domaincraft.downloader.exceptions.InvalidUserException;
import com.kronosad.domaincraft.downloader.logic.AvailableDownloads;
import com.kronosad.domaincraft.downloader.logic.CheckUpdate;
import com.kronosad.domaincraft.downloader.logic.Download;
import com.kronosad.domaincraft.downloader.logic.LoginActions;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class LaunchFrame extends JFrame{

	private static final long serialVersionUID = 1L;
	private JPanel panel = new JPanel();
	public static double VERSION = 1.0;
	private static JTextField txtUsername;
	private static JPasswordField passwordField;
	private static JComboBox comboOptions;
	private static JButton btnLogin;
	private String loginResults;
	private int attempts = 0;
	
	public static void main(String[] args){
		System.out.println("Setting look and feel to " + UIManager.getSystemLookAndFeelClassName());
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			JOptionPane.showMessageDialog(null, "Warning:\n Your OS doesn't have a supported look, the look of this" +
                    " application may look weird...");
            e.printStackTrace();
		}
		LaunchFrame launcher = new LaunchFrame();
		launcher.setVisible(true);
	}
	
	/**
	 * Initialize the frame and add all controls.
	 */
	public LaunchFrame(){
		setResizable(false);

        CheckUpdate checker = new CheckUpdate(VERSION);
        try{


            if(checker.needsUpdate()){
                if(checker.isCritical()){
                    JOptionPane.showMessageDialog(null, "Critical Update Found! Please redownload!!");
                    System.exit(1);
                }else{
                    setTitle("DomainCraft Downloader Version Update Found: " + checker.getLatestVersion());
                }
            }else{
                setTitle("DomainCraft Downloader Version: " + VERSION);
            }
        }catch(IOException e){
            JOptionPane.showMessageDialog(null, "Error checking latest version!", "Update error!", JOptionPane.ERROR);
            e.printStackTrace();
        }

		
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		setBounds(100, 100, 535, 214);
		getContentPane().setLayout(null);
		
		comboOptions = new JComboBox();
		comboOptions.setEnabled(false);
		comboOptions.setBounds(6, 137, 174, 36);
		getContentPane().add(comboOptions);
		
		
		
		
		final JButton btnDownload = new JButton("Download");
		btnDownload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Download download = new Download((String) comboOptions.getSelectedItem());
				try {
					download.startDownload();
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "Could not complete download check console...", "Download error!", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
				
			}
		});
		btnDownload.setEnabled(false);
		btnDownload.setBounds(336, 141, 117, 29);
		getContentPane().add(btnDownload);
		
		comboOptions.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				System.out.println("Test!");
				btnDownload.setEnabled(true);
				
			}
		});
		
		txtUsername = new JTextField();
		txtUsername.setBounds(16, 28, 202, 28);
		getContentPane().add(txtUsername);
		txtUsername.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(256, 28, 139, 28);
		getContentPane().add(passwordField);
		
		final JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(21, 6, 75, 16);
		getContentPane().add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(256, 6, 75, 16);
		getContentPane().add(lblPassword);
		
		btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				LoginActions login = new LoginActions(txtUsername.getText(), passwordField.getText());
				String[] result;
				
				
				if(!(attempts <= 5)){
					JOptionPane.showMessageDialog(null, "You have exhausted your login attempts!", "Error!", JOptionPane.ERROR_MESSAGE);
					JOptionPane.showMessageDialog(null, "You can create an account at: http://auth.locklin.info/", "Signup?", JOptionPane.INFORMATION_MESSAGE);
					JOptionPane.showMessageDialog(null, "Now closing.", "Warning!", JOptionPane.WARNING_MESSAGE);
					System.exit(1);
				}else{
				System.out.println("Attempt: " + attempts);
					try {
					loginResults = login.doInBackground();
				} catch (IOException e) {
					System.out.println("There was an error connecting to DomainCraft Login Servers!!");
					JOptionPane.showMessageDialog(null, "There was an error connecting to DomainCraft Login Servers!", "Connection Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				if(!(loginResults.equals("Incorrect Credentials or User Not Active"))){
					result = loginResults.split(":");
					try {
						doLogin();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					try {
						throw new InvalidUserException(loginResults);
					} catch (InvalidUserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				attempts++;
				}
			
			}
		});
		btnLogin.setBounds(407, 29, 117, 29);
		getContentPane().add(btnLogin);
	}
	/**
	 * Disable controls after successful login.
	 * @throws Exception - In case of an Internet Error
	 */
	public static void doLogin() throws Exception{
		txtUsername.setEnabled(false);
		passwordField.setEnabled(false);
		comboOptions.setEnabled(true);
		btnLogin.setEnabled(false);
		
		System.out.println("Trying to get downloads...");
		AvailableDownloads dlList = new AvailableDownloads();
		String[] downloads = dlList.getAvailable();
		
		for(int x = 0; x < downloads.length; x++){
			comboOptions.addItem(downloads[x]);
			System.out.println(downloads[x]);
		}
		
	}
	
	public static void tooManyAttempts(){
		System.out.println("Too many invalid attempts!");
		JOptionPane.showMessageDialog(null, "Too many invalid attempts!", "Invalid Credentials", JOptionPane.ERROR_MESSAGE);

	}
}
