package com.kronosad.domaincraft.downloader.exceptions;

import javax.swing.JOptionPane;

public class InvalidUserException extends Exception {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Throw plain exception
	 */
	
	public InvalidUserException(){
		super("Invalid Username or Password");
		JOptionPane.showMessageDialog(null, "You entered an invalid username or password / credentials aren't active!", "User Error", JOptionPane.ERROR_MESSAGE);

	}
	
	
	/**
	 * Throw exception with returned server message
	 */
	public InvalidUserException(String message){
		super("Invalid Username or Password, server returned message: " + message);
		JOptionPane.showMessageDialog(null, "You entered an invalid username or password, server returned message: \n" + message, "User Error", JOptionPane.ERROR_MESSAGE);

	}
}
