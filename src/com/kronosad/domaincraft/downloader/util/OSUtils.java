package com.kronosad.domaincraft.downloader.util;

public class OSUtils {

	public static OS getCurrentOS() {
		String osString = System.getProperty("os.name").toLowerCase();
		if (osString.contains("win")) {
			return OS.WINDOWS;
		} else if (osString.contains("nix") || osString.contains("nux")) {
			return OS.UNIX;
		} else if (osString.contains("mac")) {
			return OS.MACOSX;
		} else {
			return OS.OTHER;
		}
	}

	public enum OS {
		WINDOWS,
		UNIX,
		MACOSX,
		OTHER,
	}
}
