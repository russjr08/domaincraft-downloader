package com.kronosad.domaincraft.downloader.logic;

import com.kronosad.api.internet.ReadURL;

public class LoginActions{
	
	private static String username, password;
	
	/***
	 * Constructor for LoginActions, processes username and password
	 * @param username - The user's username
	 * @param password - The user's password
	 */
	public LoginActions(String username, String password){
		LoginActions.username = username;
		LoginActions.password = password;
	}
	
	/***
	 * Contacts the login server with the user's login credentials.
	 * @return result - The result of the login server
	 * @throws Exception - In case of any connection errors
	 */
	public String doInBackground() throws Exception {
		ReadURL urlRead = new ReadURL("http://auth.kronosad.com/game/getversion.php?user=" + username 
				+ "&password=" + password + "&proxy=2");
		String result = urlRead.read();
		System.out.println(result);
		return result;
	}
	
	

}
