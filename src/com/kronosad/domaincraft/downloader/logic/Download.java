package com.kronosad.domaincraft.downloader.logic;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;

import com.kronosad.domaincraft.downloader.util.OSUtils;
import com.kronosad.domaincraft.downloader.util.OSUtils.OS;


public class Download {
	public String downloadName;
	
	
	public URL repoLocation;	
	
	public File getDestination(){
		
		OS os = OSUtils.getCurrentOS();
		StringBuilder builder = new StringBuilder();
		File destination;
		if(os == OS.MACOSX){
			
			builder.append(System.getProperty("user.home")).append("/Library/Application Support/minecraft/").append(downloadName);
			destination = new File(builder.toString());
			return destination;
		}else if(os == OS.WINDOWS){
			builder.append(System.getProperty("user.home")).append("\\Application Data\\.minecraft\\").append(downloadName);
			destination = new File(builder.toString());
			return destination;
		}else if(os == OS.UNIX){
			builder.append(System.getProperty("user.home")).append("/.minecraft/").append(downloadName);
			destination = new File(builder.toString());
			return destination;
		}else{
			JOptionPane.showMessageDialog(null, "Your Operating System is not supported!", "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
	}
	
	public Download(String name){
		downloadName = name + ".zip";
	}
	
	
	public void startDownload() throws IOException{
		repoLocation = new URL("http://repo.kronosad.com/Minecraft/" + downloadName);
		//System.out.println(getDestination());
		JOptionPane.showMessageDialog(null, "The download will begin now, do not close until the next dialog box!", "Warning", JOptionPane.WARNING_MESSAGE);
		FileUtils.copyURLToFile(repoLocation, getDestination());
		JOptionPane.showMessageDialog(null, "The download has completed successfully!");
	}
	
}
