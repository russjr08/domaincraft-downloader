package com.kronosad.domaincraft.downloader.logic;


import com.kronosad.api.internet.ReadURL;

import java.io.IOException;

public class CheckUpdate {

    double currentVersion, latestVersion;

    public CheckUpdate(double currentVersion){
        this.currentVersion = currentVersion;
    }

    public boolean needsUpdate() throws IOException{
        ReadURL urlReader = new ReadURL("http://repo.kronosad.com/Minecraft/DownloaderVersion.txt");
        try{
            latestVersion = Double.parseDouble(urlReader.read());
            if(latestVersion != currentVersion){
                System.out.println("Found a new release: " + latestVersion);
                return true;
            }

        }catch(Exception e){
             e.printStackTrace();
        }
       return false;
    }


    public boolean isCritical() throws IOException{
        ReadURL urlReader = new ReadURL("http://repo.kronosad.com/Minecraft/DownloaderCritical.txt");
        try{
           double criticalReleaseNum = Double.parseDouble(urlReader.read());
            if(criticalReleaseNum != currentVersion){
                System.out.println("Found critical update: " + criticalReleaseNum + "!");
                return true;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public double getLatestVersion(){
        return latestVersion;
    }

}
